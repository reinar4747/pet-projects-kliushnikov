package characterCheetDnD.service;

import characterCheetDnD.dto.CalculatorDto;
import characterCheetDnD.models.Calculator;
import characterCheetDnD.repositories.CalculatorRepository;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.event.annotation.BeforeTestMethod;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
class CalculatorServiceImplTest {

    CalculatorService calculatorService;

    CalculatorRepository calculatorRepository;

    @Autowired
    public CalculatorServiceImplTest(CalculatorService calculatorService, CalculatorRepository calculatorRepository) {
        this.calculatorService = calculatorService;
        this.calculatorRepository = calculatorRepository;
    }

    @BeforeEach
    void setUp() {
        calculatorRepository.setDefaultAbilityScoreValues(1L);
    }

    @AfterEach
    void tearDown() {
        calculatorRepository.setDefaultAbilityScoreValues(1L);
    }



    public static Stream<List<Calculator>> strForTesting() {
        return Stream.of(Arrays.asList(Entities.calculatorStr1, Entities.calculatorStr2, Entities.calculatorStr3));
    }

    @Test
    void strengthButton() {
        CalculatorDto mockedButton = CalculatorDto.builder()
                .strengthButton("+")
                .build();
        Calculator calculatorStrength = Calculator.builder()
                .id(1L)
                .abilityScorePoints(15)
                .strengthValue(8)
                .strengthPrefix("-")
                .strengthModifier(1)
                .build();


        calculatorService.strengthButton(mockedButton, calculatorStrength,1L);
        Calculator actualResult = calculatorService.getAllById(1L).get(0);

        Assertions.assertEquals(Entities.EXPECTED1, actualResult);

    }

}