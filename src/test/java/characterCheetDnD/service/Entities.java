package characterCheetDnD.service;

import characterCheetDnD.models.Calculator;
import lombok.Data;

@Data
public class Entities {
    public static final Calculator calculatorStr1 = Calculator.builder()
            .id(1L)
            .abilityScorePoints(15)
            .strengthValue(8)
            .strengthPrefix("-")
            .strengthModifier(1)
            .build();
    public static final Calculator calculatorStr2 = Calculator.builder()
            .id(1L)
            .abilityScorePoints(13)
            .strengthValue(12)
            .strengthPrefix("+")
            .strengthModifier(1)
            .build();
    public static final Calculator calculatorStr3 = Calculator.builder()
            .id(1L)
            .abilityScorePoints(8)
            .strengthValue(15)
            .strengthPrefix("+")
            .strengthModifier(2)
            .build();

    public static final Calculator EXPECTED1 = Calculator.builder()
            .id(1L)
            .abilityScorePoints(14)

            .strengthValue(9)
            .strengthPrefix("-")
            .strengthModifier(1)

            .dexterityValue(10)
            .dexterityPrefix("+")
            .dexterityModifier(0)

            .constitutionValue(10)
            .constitutionPrefix("+")
            .constitutionModifier(0)

            .intelligenceValue(10)
            .intelligencePrefix("+")
            .intelligenceModifier(0)

            .wisdomValue(10)
            .wisdomPrefix("+")
            .wisdomModifier(0)

            .charismaValue(10)
            .charismaPrefix("+")
            .charismaModifier(0)
            .build();
}
