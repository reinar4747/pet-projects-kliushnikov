package characterCheetDnD.controllers.CRUDControllers;

import characterCheetDnD.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/deleteUserMessage")
public class DeleteUserController {

    AccountService accountService;

    @Autowired
    public DeleteUserController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping
    public String getDeletedUserControllerPage(Model model) {
        List<String> deletedUser = new ArrayList<>();
        deletedUser.add(accountService.getLoggedUser().get(0).getNickName());

        model.addAttribute("deletedUsers", deletedUser);

        return "/messages/deleteUserMessage";
    }
}
