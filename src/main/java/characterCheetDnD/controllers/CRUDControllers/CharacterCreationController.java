package characterCheetDnD.controllers.CRUDControllers;

import characterCheetDnD.dto.CharacterCRUDDto;
import characterCheetDnD.models.Armor;
import characterCheetDnD.models.Weapon;
import characterCheetDnD.service.AccountService;
import characterCheetDnD.service.CalculatorService;
import characterCheetDnD.service.CharacterCRUDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/characterNPCCreation")
public class CharacterCreationController {
    CalculatorService calculatorService;
    CharacterCRUDService characterCRUDService;
    AccountService accountService;

    @Autowired
    public CharacterCreationController(CalculatorService calculatorService,
                                       CharacterCRUDService characterCRUDService,
                                       AccountService accountService) {
        this.calculatorService = calculatorService;
        this.characterCRUDService = characterCRUDService;
        this.accountService = accountService;
    }

    @GetMapping
    public String getCharacterCreationPage(Model model) {
        model.addAttribute("loggedUser", accountService.getLoggedUser());
        model.addAttribute("characterList", characterCRUDService.getDefaultCharacter());
        model.addAttribute("calculator", calculatorService.getAllById(1L));

        return "createModels/characterNPCCreation";
    }

    @PostMapping
    public String saveCharacter(CharacterCRUDDto characterCRUDDto) {

        if (characterCRUDDto.getSaveCharacter() != null) {
            characterCRUDService.createNewCharacter();
            characterCRUDService.onAndOffCharacterById(1L, true);

            return "redirect:/characterCreatedMessage";
        } else if (characterCRUDDto.getDeleteNewCharacter() != null){
            characterCRUDService.onAndOffCharacterById(1L,true);
            switch (characterCRUDDto.getDeleteNewCharacter()) {
                case "master" -> {
                    return "redirect:/accountMaster";
                }
                case "gamer" -> {
                    return "redirect:/accountGamer";
                }
                default -> {}
            }
        } else if (characterCRUDDto.getCharacterName() != null) {
            characterCRUDService.setNameIntoDB(characterCRUDDto.getCharacterName(),1L);

            return "redirect:/characterNPCCreation";
        }

        return "";
    }

    //-------------------------------- race and class --------------------------
    @GetMapping("/raceSelection")
    public String getSelectRacePage() {
        return "createModels/raceSelection";
    }

    @GetMapping("/classSelection")
    public String getSelectClassPage() {
        return "createModels/classSelection";
    }

    @PostMapping("/raceSelection")
    public String selectRace(CharacterCRUDDto characterCRUDDto) {

        switch (characterCRUDDto.getSelectRace()) {
            case "dwarf" -> characterCRUDService.setRaceIntoDB("Дворф");
            case "human" -> characterCRUDService.setRaceIntoDB("Человек");
            case "elf" -> characterCRUDService.setRaceIntoDB("Эльф");
        }

        return "redirect:/characterNPCCreation";
    }

    @PostMapping("/classSelection")
    public String selectClass(CharacterCRUDDto characterCRUDDto) {
        switch (characterCRUDDto.getSelectClass()) {
            case "warrior" -> characterCRUDService.setClassIntoDB("Воин");
            case "rogue" -> characterCRUDService.setClassIntoDB("Разбойник");
            case "ranger" -> characterCRUDService.setClassIntoDB("Следопыт");
        }

        return "redirect:/characterNPCCreation";
    }

    // ------------------------- weapon and armor ---------------------
    @GetMapping("/weaponSelection")
    public String getSelectWeaponPage(Model model) {
        switch (characterCRUDService.getDefaultCharacter().get(0).getClassName()) {
            case "-" -> {
                return "redirect:/characterNPCCreation/weaponSelection/characterCreationError";
            }
            default -> {
                switch (characterCRUDService.getDefaultCharacter().get(0).getClassName()) {
                    case "Воин" -> {
                        List<Weapon> allWeapons = new ArrayList<>();
                        allWeapons.addAll(characterCRUDService.getWeaponByEquipped("1h"));
                        allWeapons.addAll(characterCRUDService.getWeaponByEquipped("2h"));

                        model.addAttribute("weapons", allWeapons);
                    }
                    case "Следопыт" -> {
                        List<Weapon> allWeapons = new ArrayList<>();
                        allWeapons.addAll(characterCRUDService.getWeaponByEquipped("1h"));
                        allWeapons.addAll(characterCRUDService.getAllRangedWeapon());

                        model.addAttribute("weapons", allWeapons);
                    }
                    case "Разбойник" -> model.addAttribute("weapons", characterCRUDService.getWeaponByWeight("light"));
                }
            }
        }
        return "createModels/weaponSelection";
    }

    @GetMapping("/armourSelection")
    public String getSelectArmorPage(Model model) {

        List<Armor> heavy = new ArrayList<>();
        heavy.addAll(characterCRUDService.getAllByArmorWeight("heavy"));
        heavy.addAll(characterCRUDService.getAllByArmorWeight("mid"));
        heavy.addAll(characterCRUDService.getAllByArmorWeight("light"));
        model.addAttribute("armors", heavy);

        List<Armor> mid = new ArrayList<>();
        mid.addAll(characterCRUDService.getAllByArmorWeight("mid"));
        mid.addAll(characterCRUDService.getAllByArmorWeight("light"));

        switch (characterCRUDService.getDefaultCharacter().get(0).getClassName()) {
            case "-" -> {
                return "redirect:/characterNPCCreation/weaponSelection/characterCreationError";
            }
            default -> {
                switch (characterCRUDService.getDefaultCharacter().get(0).getClassName()) {
                 //   case "Воин" -> model.addAttribute("armors", characterCRUDService.getAllByArmorWeight("heavy"));
                    case "Воин" -> model.addAttribute("armors", heavy);
                 //   case "Следопыт" -> model.addAttribute("armors", characterCRUDService.getAllByArmorWeight("mid"));
                    case "Следопыт" -> model.addAttribute("armors", mid);
                    case "Разбойник" -> model.addAttribute("armors", characterCRUDService.getAllByArmorWeight("light"));
                }
            }
        }

        return "createModels/armourSelection";
    }

    @PostMapping("/weaponSelection")
    @Transactional
    public String selectWeapon(CharacterCRUDDto characterCRUDDto) {

        characterCRUDService.setWeaponIntoDB(
                characterCRUDService.getWeaponById(characterCRUDDto.getSelectWeapon()).get(0).getName(), 1L);

        characterCRUDService.getDefaultCharacter()
                .get(0)
                .getWeapons()
                .add(characterCRUDService.getWeaponById(characterCRUDDto.getSelectWeapon()).get(0));

        return "redirect:/characterNPCCreation";

    }

    @PostMapping("/armourSelection")
    @Transactional
    public String selectArmor(CharacterCRUDDto characterCRUDDto) {
        characterCRUDService.setArmourIntoDB(
                characterCRUDService.getArmorById(characterCRUDDto.getSelectArmor()).get(0).getArmorName(), 1L);

        characterCRUDService.getDefaultCharacter()
                .get(0)
                .getArmors()
                .add(characterCRUDService.getArmorById(characterCRUDDto.getSelectArmor()).get(0));

        return "redirect:/characterNPCCreation";

    }


}


