package characterCheetDnD.controllers.entitiesControllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import characterCheetDnD.dto.CalculatorDto;
import characterCheetDnD.models.Calculator;
import characterCheetDnD.service.CalculatorService;

import java.util.List;

@Controller
@RequestMapping("/calculator")
public class CalculatorController {

    private final CalculatorService calculatorService;
    Calculator calculatorAbilityScores;

    @Autowired
    public CalculatorController(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;
    }

    @GetMapping
    public String getCalculatorPage(Model model) {

        List<Calculator> calculator = calculatorService.getAllById(1L);
        model.addAttribute("calculator", calculator);
        calculatorAbilityScores = calculator.get(0);

        return "createModels/calculator";
    }

    @PostMapping
    public String strengthCalculator(CalculatorDto calculatorDto) {
        if (calculatorDto.getStrengthButton() != null) {
            calculatorService.strengthButton(calculatorDto, calculatorAbilityScores,1L);

        } else if (calculatorDto.getDexterityButton() != null) {
            calculatorService.dexterityButton(calculatorDto, calculatorAbilityScores,1L);

        } else if (calculatorDto.getConstitutionButton() != null) {
            calculatorService.constitutionButton(calculatorDto, calculatorAbilityScores,1L);

        } else if (calculatorDto.getIntelligenceButton() != null) {
            calculatorService.intelligenceButton(calculatorDto, calculatorAbilityScores,1L);

        } else if (calculatorDto.getWisdomButton() != null) {
            calculatorService.wisdomButton(calculatorDto, calculatorAbilityScores,1L);

        } else if (calculatorDto.getCharismaButton() != null) {
            calculatorService.charismaButton(calculatorDto, calculatorAbilityScores,1L);

        } else if (calculatorDto.getSaveAbilityScores().equals("save")) {

            return "redirect:/characterNPCCreation";

        } else if (calculatorDto.getSaveAbilityScores().equals("delete")) {
            calculatorService.setDefaultAbilityScoreValues(1L);

            return "redirect:/characterNPCCreation";
        }

        return "redirect:/calculator";
    }


}
