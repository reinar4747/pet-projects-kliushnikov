package characterCheetDnD.controllers.entitiesControllers;

import characterCheetDnD.dto.AccountChangeDataDto;
import characterCheetDnD.models.UserAccount;
import characterCheetDnD.service.AccountService;
import characterCheetDnD.service.CalculatorService;
import characterCheetDnD.service.CharacterCRUDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/accountGamer")
public class AccountGamerController {

    CalculatorService calculatorService;
    private final AccountService accountService;
    CharacterCRUDService characterCRUDService;

    @Autowired
    public AccountGamerController(CalculatorService calculatorService,
                                  AccountService accountService,
                                  CharacterCRUDService characterCRUDService) {
        this.calculatorService = calculatorService;
        this.accountService = accountService;
        this.characterCRUDService = characterCRUDService;
    }




    @GetMapping
    public String getaAccountGamerPage(Model model) {
        calculatorService.setDefaultAbilityScoreValues(1L);

        List<String> gamerNickName = new ArrayList<>();
        gamerNickName.add( accountService.getUserById(accountService.getLoggedUser().get(0).getId(),false).get(0).getNickName());

        model.addAttribute("gamerNickName", gamerNickName);
        return "modelPages/accountGamer";
    }

    @PostMapping
    public String gamerAccountChange(AccountChangeDataDto accountChangeDataDto) {

        if (accountChangeDataDto.getDeleteUser() != null) {
            accountService.deleteUserById(accountService.getLoggedUser().get(0).getId());

            return "redirect:/deleteUserMessage";
        } else if (accountChangeDataDto.getCreate() != null) {
            characterCRUDService.onAndOffCharacterById(1L, false);
            characterCRUDService.setDefaultCharactersValues();

            return "redirect:/characterNPCCreation";
        }


        return "modelPages/accountMaster";
    }


}
