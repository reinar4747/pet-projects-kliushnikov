package characterCheetDnD.controllers.utilityControllers;

import characterCheetDnD.dto.CharacterCRUDDto;
import characterCheetDnD.service.AccountService;
import characterCheetDnD.service.CharacterCRUDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/characterSelect")
public class CharacterSelectController {
    AccountService accountService;
    CharacterCRUDService characterCRUDService;

    @Autowired
    public CharacterSelectController(AccountService accountService, CharacterCRUDService characterCRUDService) {
        this.accountService = accountService;
        this.characterCRUDService = characterCRUDService;
    }

    @GetMapping
    public String getCharacterSelectionPage(Model model) {
        model.addAttribute("loggedUser", accountService.getLoggedUser());

        model.addAttribute("characterList",
                characterCRUDService.getCharactersByUserId(accountService.getLoggedUser().get(0)));

        return "/serviceModels/characterSelect";
    }

    @PostMapping
    public String getBackAccountPage(CharacterCRUDDto characterCRUDDto) {
        if (characterCRUDDto.getBackToAccount() != null){
            switch (characterCRUDDto.getBackToAccount()) {
                case "master" -> {
                    return "redirect:/accountMaster";
                }
                case "gamer" -> {
                    return "redirect:/accountGamer";
                }
                default -> {}
            }
        } else if (characterCRUDDto.getChowCharacter() != 0L) {
            characterCRUDService.saveCharacter(characterCRUDDto.getChowCharacter());

            return "redirect:/characterSelect/characterSheetLook";
        } else if (characterCRUDDto.getDeleteCharacter() != 0L) {

            characterCRUDService.logicalDeleteCharacterById(true, characterCRUDDto.getDeleteCharacter());

        }

        return "redirect:/characterSelect";
    }
}
