package characterCheetDnD.controllers.utilityControllers;

import characterCheetDnD.dto.MonsterDto;
import characterCheetDnD.service.AccountService;
import characterCheetDnD.service.MonsterCRUDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/accountMaster/monsterSelect")
public class monsterSelectController {
    AccountService accountService;
    MonsterCRUDService monsterCRUDService;
    String monsterFilter = "-";

    @Autowired
    public monsterSelectController(AccountService accountService, MonsterCRUDService monsterCRUDService) {
        this.accountService = accountService;
        this.monsterCRUDService = monsterCRUDService;
    }

    @GetMapping
    public String getMonsterSelectionPage(Model model) {
        model.addAttribute("loggedUser", accountService.getLoggedUser());
        //   model.addAttribute("monsterList", monsterCRUDService.getMonsterByUserId(accountService.getLoggedUser().get(0)));
        switch (monsterFilter) {
            case "human" -> model.addAttribute("monsterList",
                    monsterCRUDService.getAllByUserAccountAndMonsterType(accountService.getLoggedUser().get(0), "Гуманоид"));
            case "undead" -> model.addAttribute("monsterList",
                    monsterCRUDService.getAllByUserAccountAndMonsterType(accountService.getLoggedUser().get(0), "Нежить"));
            case "beast" -> model.addAttribute("monsterList",
                    monsterCRUDService.getAllByUserAccountAndMonsterType(accountService.getLoggedUser().get(0), "Зверь"));
            default ->
                    model.addAttribute("monsterList", monsterCRUDService.getMonsterByUserId(accountService.getLoggedUser().get(0)));
        }


        return "/serviceModels/monsterSelect";
    }

    @PostMapping
    public String getBackAccountPageFromMonster(MonsterDto monsterDto){
        if (monsterDto.getBackToMasterPage() != null){

            return "redirect:/accountMaster";
        } else if (monsterDto.getChowMonster() != 0L) {
            monsterCRUDService.saveMonster(monsterDto.getChowMonster());

            return "redirect:/accountMaster/monsterSelect/monsterSheetLook";
        } else if (monsterDto.getDeleteMonster() != 0L) {
            monsterCRUDService.logicalDeleteMonsterById(true, monsterDto.getDeleteMonster());
        } else if (monsterDto.getMonsterSelect() != null) {
            switch (monsterDto.getMonsterSelect()) {
                case "human" -> monsterFilter = "human";
                case "undead" -> monsterFilter = "undead";
                case "beast" -> monsterFilter = "beast";
                case "-" -> monsterFilter = "-";
            }

        }

        return "redirect:/accountMaster/monsterSelect";
    }
}
