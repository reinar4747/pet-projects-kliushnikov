package characterCheetDnD.controllers.utilityControllers;

import characterCheetDnD.dto.AccountDto;
import characterCheetDnD.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/signUp")
public class SignUpController {

    private final AccountService accountService;

    @Autowired
    public SignUpController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping//("/signUp") //регистрация
    public String getSignUpPage() {
        return "signUp";
    }

    @PostMapping
    public String signUp(AccountDto accountDto) {

        if (accountService.signUp(accountDto).size() == 0) {
            if (accountDto.getUserRole().equals("master")) {
                return "/masterCreated";
            } else {
                return "/gamerCreated";
            }
        } else {
            return "redirect:/signUpError";
        }
    }


}
