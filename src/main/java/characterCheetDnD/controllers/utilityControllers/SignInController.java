package characterCheetDnD.controllers.utilityControllers;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import characterCheetDnD.dto.SignInDto;
import characterCheetDnD.models.UserAccount;
import characterCheetDnD.service.AccountService;

import java.util.List;
@Data
@Controller
@RequestMapping("/signIn")
public class SignInController {

  //  private List<UserAccount> loggedUser;
    private final AccountService accountService;

    @Autowired
    public SignInController(AccountService accountService) {
        this.accountService = accountService;

    }

    @GetMapping
    public String getSignInPage() {
        return "signIn";
    }

    @PostMapping//("/signIn")
    public String logIn(SignInDto signInDto) {
        List<UserAccount> userLogin = accountService
                .getAllAccountsByEmailAndPassword(signInDto.getEmail(), signInDto.getPassword());
        if (userLogin.size() == 1) {
            accountService.getLoggedUser().clear();
            accountService.getLoggedUser().add(userLogin.get(0));
           int a =1 ;
        //    loggedUser = userLogin;
            if (userLogin.get(0).getUserRole().equals("master")) {

                return "redirect:/accountMaster";
            } else {

                return "redirect:/accountGamer";
            }
        } else {
            return "errors/signInError";
        }
    }


//    @GetMapping("/accountMaster")
//    public String getAccountMaster(){
//        return "modelPages/accountMaster";
//    }
//    @GetMapping("/accountGamer")
//    public String getAccountGamer(){
//        return "modelPages/accountGamer";
//    }


//    @RequestMapping(value = "/redirect", method = RequestMethod.GET)
//    public String redirect() {
//
//        return "redirect:finalPage";
//    }


}
