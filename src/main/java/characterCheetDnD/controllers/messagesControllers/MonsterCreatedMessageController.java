package characterCheetDnD.controllers.messagesControllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/monsterCreatedMessage")
public class MonsterCreatedMessageController {

    @GetMapping
    public String getMonsterCreatedMessagePage(){
        return "/messages/monsterCreatedMessage";
    }
    @PostMapping
    public String getAccountPage(){
        return "redirect:/accountMaster";
    }
}
