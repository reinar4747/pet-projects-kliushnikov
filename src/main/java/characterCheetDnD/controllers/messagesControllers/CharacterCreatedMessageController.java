package characterCheetDnD.controllers.messagesControllers;

import characterCheetDnD.dto.CharacterCRUDDto;
import characterCheetDnD.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/characterCreatedMessage")
public class CharacterCreatedMessageController {

    AccountService accountService;

    @Autowired
    public CharacterCreatedMessageController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping
    public String getMessagePage(Model model) {
        model.addAttribute("loggedUser", accountService.getLoggedUser());
        return "/messages/characterCreatedMessage";
    }
    @PostMapping
    public String getBackAccountPage(CharacterCRUDDto characterCRUDDto){
         switch (characterCRUDDto.getBackToAccount()){
            case "master" -> {
                return "redirect:/accountMaster";
            }
            case "gamer" -> {
                return "redirect:/accountGamer";
            }
        }
        return null;
    }


}
