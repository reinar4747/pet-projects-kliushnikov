package characterCheetDnD.controllers.errorsControllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/characterNPCCreation/weaponSelection/characterCreationError")
public class ArmorAndWeaponErrorController {

    @GetMapping
    public String getArmorAndWeaponError() {
        return "/errors/characterCreationError";
    }
}
