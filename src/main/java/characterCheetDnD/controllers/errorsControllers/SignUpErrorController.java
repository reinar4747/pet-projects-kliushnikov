package characterCheetDnD.controllers.errorsControllers;

import characterCheetDnD.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/signUpError")
public class SignUpErrorController {

    private final AccountService accountService;

    @Autowired
    public SignUpErrorController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping
    public String getAllErrors(Model model) {

        List<String> errors = accountService.getAllErrors();
        model.addAttribute("errors", errors);

        return "errors/signUpError";
    }
}
