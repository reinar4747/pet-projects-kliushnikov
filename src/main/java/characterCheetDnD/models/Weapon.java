package characterCheetDnD.models;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "weapon")
@ToString(exclude = "gameCharacters")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Weapon {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "weapon_id")
    long weaponId;
    @Column(name = "weapon_name")
    String name;
    @Column(name = "weapon_dmg")
    String damage;
    @Column(name = "equipped")
    String equipped;
    @Column(name = "range")
    String range;
    @Column(name = "weapon_weight")
    String weight;
    @ManyToMany(mappedBy = "weapons")
    @Builder.Default
    List<GameCharacter> gameCharacters = new ArrayList<>();
}
