package characterCheetDnD.models;

import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "game_monster")
@ToString(exclude = "userAccounts")
public class Monster {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "monster_id")
    private long id;
    @Column(name = "monster_name")
    private String monsterName;
    @Column(name = "monster_type")
    private String monsterType;
    @Column(name = "monster_description")
    private String monsterDescription;
    @Column(name = "monster_str")
    private String monsterStrength;
    @Column(name = "monster_dex")
    private String monsterDexterity;
    @Column(name = "monster_con")
    private String monsterConstitution;
    @Column(name = "monster_int")
    private String monsterIntelligence;
    @Column(name = "monster_wis")
    private String monsterWisdom;
    @Column(name = "monster_cha")
    private String monsterCharisma;
    @Column(name = "monster_deleted")
    private boolean monsterDeleted;

    @ManyToMany(mappedBy = "monsters")
    @Builder.Default
    private List<UserAccount> userAccounts = new ArrayList<>();





}
