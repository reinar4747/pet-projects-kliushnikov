package characterCheetDnD.repositories;

import characterCheetDnD.models.GameCharacter;
import characterCheetDnD.models.Weapon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface WeaponRepository extends JpaRepository<Weapon, Long> {

    List<Weapon> getAllByWeaponId(long weaponId);

    List<Weapon> getAllByName(String name);

    List<Weapon> getAllByGameCharacters(GameCharacter gameCharacter);



    @Modifying
    @Query(value = "update Weapon set gameCharacters = :gameCharacter where weaponId = :weaponId")
    void setGameCharacterToWeapon(@Param("gameCharacter") GameCharacter characterId,
                                  @Param("weaponId") long weaponId);

    @Query(value = "from Weapon where range not like '-'")
    List<Weapon> getAllRangedWeapon();

    @Query(value = "from Weapon where weight = :weight")
    List<Weapon> getWeaponByWeight(@Param("weight") String weight);

    @Query(value = "from Weapon where equipped = :equipped")
    List<Weapon> getWeaponByEquipped(@Param("equipped") String equipped);


}
