package characterCheetDnD.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import characterCheetDnD.models.Calculator;

import org.springframework.transaction.annotation.Transactional;

//import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface CalculatorRepository extends JpaRepository<Calculator, Long> {


    List<Calculator> getAllById(long id);

  //  @Transactional
    @Modifying   // default
    @Query(value = "update Calculator set " +
            "abilityScorePoints = 15," +
            "strengthValue = 10, strengthPrefix = '+', strengthModifier = 0," +
            "dexterityValue = 10, dexterityPrefix = '+',dexterityModifier = 0," +
            "constitutionValue = 10,constitutionPrefix = '+',constitutionModifier = 0," +
            "intelligenceValue = 10,intelligencePrefix = '+',intelligenceModifier = 0," +
            "wisdomValue = 10, wisdomPrefix = '+',wisdomModifier = 0," +
            "charismaValue = 10,charismaPrefix = '+',charismaModifier = 0 " +
            "where id = :id")
    void setDefaultAbilityScoreValues(@Param("id") long id);// default

    //@Transactional
    @Modifying
    @Query(value = "update Calculator set " +
            "abilityScorePoints = :abilityScorePoints," +
            "strengthValue = :strValue," +
            "strengthPrefix = :strPrefix," +
            "strengthModifier = :strModifier " +
            "where id = :id") //str
    void strengthSetIntoDB(@Param("abilityScorePoints") int abilityScorePoints,
                           @Param("strValue") int strValue,
                           @Param("strPrefix") String strPrefix,
                           @Param("strModifier") int strModifier,
                           @Param("id") long id);

   // @Transactional
    @Modifying
    @Query(value = "update Calculator set " +
            "abilityScorePoints = :abilityScorePoints," +
            "dexterityValue = :dexValue," +
            "dexterityPrefix = :dexPrefix," +
            "dexterityModifier =:dexModifier " +
            "where id = :id") //dex
    void dexteritySetIntoDB(@Param("abilityScorePoints") int abilityScorePoints,
                            @Param("dexValue") int dexValue,
                            @Param("dexPrefix") String dexPrefix,
                            @Param("dexModifier") int dexModifier,
                            @Param("id") long id);

    //@Transactional
    @Modifying
    @Query(value = "update Calculator set " +
            "abilityScorePoints = :abilityScorePoints," +
            "constitutionValue = :conValue," +
            "constitutionPrefix = :conPrefix," +
            "constitutionModifier =:conModifier " +
            "where id = :id") //con
    void constitutionSetIntoDB(@Param("abilityScorePoints") int abilityScorePoints,
                               @Param("conValue") int conValue,
                               @Param("conPrefix") String conPrefix,
                               @Param("conModifier") int conModifier,
                               @Param("id") long id);

    //@Transactional
    @Modifying
    @Query(value = "update Calculator set " +
            "abilityScorePoints = :abilityScorePoints," +
            "intelligenceValue = :intValue," +
            "intelligencePrefix = :intPrefix," +
            "intelligenceModifier =:intModifier " +
            "where id = :id") //int
    void intelligenceSetIntoDB(@Param("abilityScorePoints") int abilityScorePoints,
                               @Param("intValue") int intValue,
                               @Param("intPrefix") String intPrefix,
                               @Param("intModifier") int intModifier,
                               @Param("id") long id);

    //@Transactional
    @Modifying
    @Query(value = "update Calculator set " +
            "abilityScorePoints = :abilityScorePoints," +
            "wisdomValue = :wisValue," +
            "wisdomPrefix = :wisPrefix," +
            "wisdomModifier =:wisModifier " +
            "where id = :id") //wis
    void wisdomSetIntoDB(@Param("abilityScorePoints") int abilityScorePoints,
                         @Param("wisValue") int wisValue,
                         @Param("wisPrefix") String wisPrefix,
                         @Param("wisModifier") int wisModifier,
                         @Param("id") long id);

    //@Transactional
    @Modifying
    @Query(value = "update Calculator set " +
            "abilityScorePoints = :abilityScorePoints," +
            "charismaValue = :chaValue," +
            "charismaPrefix = :chaPrefix," +
            "charismaModifier =:chaModifier " +
            "where id = :id") //cha
    void charismaSetIntoDB(@Param("abilityScorePoints") int abilityScorePoints,
                           @Param("chaValue") int chaValue,
                           @Param("chaPrefix") String chaPrefix,
                           @Param("chaModifier") int chaModifier,
                           @Param("id") long id);


}

