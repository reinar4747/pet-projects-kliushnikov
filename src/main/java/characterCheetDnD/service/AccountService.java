package characterCheetDnD.service;

import characterCheetDnD.dto.AccountChangeDataDto;
import characterCheetDnD.dto.AccountDto;
import characterCheetDnD.models.UserAccount;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface AccountService {
    List<UserAccount> getLoggedUser();
    List<UserAccount> getAllAccountsByEmailAndPassword(String email, String password);
    List<String> signUp(AccountDto accountDto);

    List<UserAccount> getUserById(long userId, boolean flag);

    void deleteUserById(long id);
    List<String> getAllErrors();
    List<String> updatePassword(AccountChangeDataDto accountChangeDataDto);
    void updateFirstName(long id,AccountChangeDataDto accountChangeDataDto);
    void updateLastName(long id,AccountChangeDataDto accountChangeDataDto);
    void updateNickName(long id,AccountChangeDataDto accountChangeDataDto);
    void updateEmail(long id,AccountChangeDataDto accountChangeDataDto);


}
