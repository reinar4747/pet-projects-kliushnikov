package characterCheetDnD.service;

import characterCheetDnD.dto.AccountChangeDataDto;
import characterCheetDnD.dto.AccountDto;
import characterCheetDnD.models.UserAccount;
import characterCheetDnD.repositories.UserAccountRepository;
import characterCheetDnD.validators.EmailValidator;
import characterCheetDnD.validators.PasswordValidator;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Data
@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    private List<UserAccount> loggedUser;
    private List<String> emailPasswordErrors;
    private final UserAccountRepository userAccountRepository;
    private final List<PasswordValidator> passwordValidators;
    private final List<EmailValidator> emailValidators;

    // @Autowired
    @Autowired
    public AccountServiceImpl(List<UserAccount> loggedUser,
                              List<String> emailPasswordErrors,
                              UserAccountRepository userAccountRepository,
                              List<PasswordValidator> passwordValidators,
                              List<EmailValidator> emailValidators) {
        this.loggedUser = loggedUser;
        this.emailPasswordErrors = emailPasswordErrors;
        this.userAccountRepository = userAccountRepository;
        this.passwordValidators = passwordValidators;
        this.emailValidators = emailValidators;
    }

    @Override //for logins
    public List<UserAccount> getAllAccountsByEmailAndPassword(String email, String password) {
        return userAccountRepository.getAllByEmailAndPasswordAndUserDeleted(email, password, false);
    }

    @Override
    public void updateFirstName(long id, AccountChangeDataDto accountChangeDataDto) {
        userAccountRepository.setNewFirstNameIntoDB(accountChangeDataDto.getNewFirstName(), id);
    }

    @Override
    public void updateLastName(long id, AccountChangeDataDto accountChangeDataDto) {
        userAccountRepository.setNewLastNameIntoDB(accountChangeDataDto.getNewLastName(), id);
    }

    @Override
    public void updateNickName(long id, AccountChangeDataDto accountChangeDataDto) {
        userAccountRepository.setNewNickNameIntoDB(accountChangeDataDto.getNewNickName(), id);
    }

    @Override
    public void updateEmail(long id, AccountChangeDataDto accountChangeDataDto) {
        userAccountRepository.setNewEmailIntoDB(accountChangeDataDto.getNewEmail(), id);
    }

    @Override
    public List<String> updatePassword(AccountChangeDataDto accountChangeDataDto) {
        emailPasswordErrors.clear();

        if (accountChangeDataDto.getOldPassword().equals(loggedUser.get(0).getPassword())) {
            passwordValidators.stream()
                    .map(passwordValidator -> passwordValidator.validate(accountChangeDataDto.getNewPassword(),
                            accountChangeDataDto.getNewPasswordRepeat()))
                    .filter(passwordValidator -> passwordValidator.length() != 0)
                    .forEach(emailPasswordErrors::add);
            if (accountChangeDataDto.getOldPassword().equals(accountChangeDataDto.getNewPassword())){
                emailPasswordErrors.add("Новый и старый пароль совпадают !!!");
            }
        } else {
            emailPasswordErrors.add("Старый пароль не правильный!!!");
        }

        if (emailPasswordErrors.size() == 0) {
            userAccountRepository.setNewPasswordIntoDB(accountChangeDataDto.getNewPassword(), loggedUser.get(0).getId());
        }

        return emailPasswordErrors;
    }


    @Override // create
    public List<String> signUp(AccountDto accountDto) {

        emailPasswordErrors.clear();

        passwordValidators.stream()
                .map(passwordValidator -> passwordValidator.validate(accountDto.getPassword(), accountDto.getPasswordRepeat()))
                .filter(passwordValidator -> passwordValidator.length() != 0)
                .forEach(emailPasswordErrors::add);

        emailValidators.stream()
                .map(emailValidator -> emailValidator.validate(accountDto.getEmail()))
                .filter(emailValidator -> emailValidator.length() != 0)
                .forEach(emailPasswordErrors::add);

        if (emailPasswordErrors.size() == 0) {
            UserAccount userAccount = UserAccount.builder()
                    .firstName(accountDto.getFirstName())
                    .lastName(accountDto.getLastName())
                    .nickName(accountDto.getNickName())
                    .userRole(accountDto.getUserRole())
                    .email(accountDto.getEmail())
                    .password(accountDto.getPassword())
                    .userDeleted(false)
                    .build();

            userAccountRepository.save(userAccount);

        }

        return emailPasswordErrors;
    }

    @Override
    public List<UserAccount> getUserById(long userId, boolean flag) {
        return userAccountRepository.getAllByIdAndUserDeleted(userId,flag);
    }

    @Override
    public void deleteUserById(long userId) {
        Optional<UserAccount> user = userAccountRepository.findById(userId);
        if (user.isPresent()) {
            user.get().setUserDeleted(true);
            userAccountRepository.save(user.get());
        } else {
            throw new IllegalArgumentException("Пользователя не существует");
        }
    }

    @Override// errors
    public List<String> getAllErrors() {
        return emailPasswordErrors;
    }


    @Override
    public List<UserAccount> getLoggedUser() {
        return loggedUser;
    }
}
