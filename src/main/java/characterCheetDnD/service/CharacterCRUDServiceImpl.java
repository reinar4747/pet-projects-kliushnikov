package characterCheetDnD.service;

import characterCheetDnD.models.*;
import characterCheetDnD.repositories.ArmorRepository;
import characterCheetDnD.repositories.CharacterRepository;
import characterCheetDnD.repositories.WeaponRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Data
@Service
public class CharacterCRUDServiceImpl implements CharacterCRUDService {
    CharacterRepository characterRepository;
    AccountService accountService;
    CalculatorService calculatorService;
    WeaponRepository weaponRepository;
    ArmorRepository armorRepository;
    List<GameCharacter> characterToShow;


    @Autowired
    public CharacterCRUDServiceImpl(CharacterRepository characterRepository,
                                    AccountService accountService,
                                    CalculatorService calculatorService,
                                    WeaponRepository weaponRepository,
                                    ArmorRepository armorRepository) {
        this.characterRepository = characterRepository;
        this.accountService = accountService;
        this.calculatorService = calculatorService;
        this.weaponRepository = weaponRepository;
        this.armorRepository = armorRepository;
    }


    @Override
    public List<GameCharacter> getDefaultCharacter() {
        return characterRepository.getAllById(1L);
    }

    @Override
    public void setDefaultCharactersValues() {
        characterRepository.setDefaultCharactersValues(accountService.getLoggedUser().get(0),
                calculatorService.getAllById(1L).get(0));
    }

    @Override
    public List<GameCharacter> getCharactersById(long characterId) {
        return characterRepository.getAllById(characterId);
    }

    @Override
    public List<GameCharacter> getCharactersByUserId(UserAccount userAccount) {
      return characterRepository.getAllByUserAccountAndCharacterDeleted(userAccount,false);
    }

    @Override
    @Transactional
    public void saveCharacter(long characterId) {
        characterToShow = getCharactersById(characterId);
    }

    @Override
    @Transactional
    public List<GameCharacter> character() {
        return characterToShow;
    }

    @Override
    @Transactional
    public void logicalDeleteCharacterById(boolean flag, long characterId) {
        characterRepository.setCharacterOnAnDOff(flag, characterId);

    }

    //------------------------ race and class -------------------------------------
    @Override
    public void setRaceIntoDB(String race) {
        characterRepository.setCharacterRaceIntoDB(race);
    }

    @Override
    public void setClassIntoDB(String characterClass) {
        characterRepository.setCharacterClassIntoDB(characterClass);
    }


    //------------------ LvL and Experience and hitPoints------------
    @Override
    public void setLvLIntoDB(int LvL, long characterId) {
        characterRepository.setLvLIntoDB(LvL, characterId);
    }

    @Override
    public void setExperienceIntoDB(int experience, long characterId) {
        characterRepository.setExperienceIntoDB(experience, characterId);
    }

    @Override
    public void setHitPointsIntoDB(int hitPoints, long characterId) {
        characterRepository.setHitPointsIntoDB(hitPoints, characterId);
    }

    //------------------------------- weapon -----------------------------

    @Override
    public List<Weapon> getWeaponById(long weaponId) {
        return weaponRepository.getAllByWeaponId(weaponId);
    }

    @Override
    public void setWeaponIntoDB(String weapon, long characterId) {
        characterRepository.setCharacterWeaponIntoDB(weapon, characterId);
    }

    @Override
    public List<Weapon> getAllRangedWeapon() {
        return weaponRepository.getAllRangedWeapon();
    }

    @Override
    public List<Weapon> getWeaponByWeight(String weight) {
        return weaponRepository.getWeaponByWeight(weight);
    }

    @Override
    public List<Weapon> getWeaponByEquipped(String equipped) {
        return weaponRepository.getWeaponByEquipped(equipped);
    }


    //---------------------------------------- armor -----------------------
    @Override
    public List<Armor> getArmorById(long armorId) {
        return armorRepository.getAllByArmorId(armorId);
    }

    @Override
    public void setArmourIntoDB(String armour,long characterId) {
        characterRepository.setCharacterArmorIntoDB(armour,characterId);
    }

    @Override
    public List<Armor> getAllByArmorWeight(String weight) {
        return armorRepository.getAllByArmorWeight(weight);
    }

    //-------------------------------name -----------------------------------------
    @Override
    public void setNameIntoDB(String name,long charId) {
        characterRepository.setCharacterNameIntoDB(name,charId);
    }

    //    --------------------------------- create --------------------
    @Override
    @Transactional
    public void createNewCharacter() {

        GameCharacter tempCharacter = getDefaultCharacter().get(0);
        int tempModifier;

        switch (tempCharacter.getAbilityScores().getConstitutionPrefix()) {
            case "-" -> tempModifier = tempCharacter.getAbilityScores().getConstitutionModifier() * -1;
            default -> tempModifier = tempCharacter.getAbilityScores().getConstitutionModifier();
        }
        int tempHp = 0;

        switch (tempCharacter.getClassName()) {
            case "Воин" -> tempHp = 12 + tempModifier;
            case "Следопыт" -> tempHp = 10 + tempModifier;
            case "Разбойник" -> tempHp = 8 + tempModifier;
        }

        Calculator newStats = calculatorService.saveAbilityScores(1L);

        int a = 1;

        GameCharacter newGameCharacter = GameCharacter.builder()
                .name(tempCharacter.getName())
                .race(tempCharacter.getRace())
                .className(tempCharacter.getClassName())
                .abilityScores(newStats)
                .experience(0)
                .hitPoints(tempHp)
                .maxHitPoints(tempHp)
                .level(1)
                .weapon(tempCharacter.getWeapon())
                .armour(tempCharacter.getArmour())
                .weapons(weaponRepository.getAllByWeaponId(tempCharacter.getWeapons().get(0).getWeaponId()))
                .armors(armorRepository.getAllByArmorId(tempCharacter.getArmors().get(0).getArmorId()))
                .characterDeleted(false)
                .inventory("-")
                .userAccount(accountService.getLoggedUser().get(0))
                .build();


        characterRepository.save(newGameCharacter);
    }


    @Override
    public void onAndOffCharacterById(long characterId, boolean flag) {
        Optional<GameCharacter> gameCharacter = characterRepository.findById(characterId);
        if (gameCharacter.isPresent()) {
            gameCharacter.get().setCharacterDeleted(flag);
            gameCharacter.get().getWeapons().clear();
            gameCharacter.get().getArmors().clear();
            characterRepository.save(gameCharacter.get());
        } else {
            throw new IllegalArgumentException("Пользователя не существует");
        }

    }
//    ------------------------------inventory-----------------------------------
    @Override
    public void addItemIntoInventory(String newItem, long characterId) {

        characterRepository.setInventoryIntoDB(newItem + "|",characterId);
    }



}
