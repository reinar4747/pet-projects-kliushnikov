package characterCheetDnD.service;

import characterCheetDnD.dto.CalculatorDto;
import characterCheetDnD.models.Calculator;
import characterCheetDnD.repositories.CalculatorRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Data
@Service
@Transactional
public class CalculatorServiceImpl implements CalculatorService {

    CalculatorRepository calculatorRepository;

    @Autowired
    public CalculatorServiceImpl(CalculatorRepository calculatorRepository) {
        this.calculatorRepository = calculatorRepository;
    }

    private int abilityCostPlus(int abilityValue) {
        if (abilityValue >= 8 && abilityValue < 13) {
            return 1;
        } else if (abilityValue >= 13 && abilityValue < 15) {
            return 2;
        } else if (abilityValue >= 15) {
            return 3;
        }
        return 1;
    }

    private int abilityCostMinus(int abilityValue) {
        return switch (abilityValue) {
            case 8, 9, 12, 13 -> 1;
            case 14, 15 -> 2;
            case 16, 17, 18, 19 -> 3;
            default -> 1;
        };
    }

    private int getAbilityModifier(int abilityValue) {
        return switch (abilityValue) {
            case 10, 11 -> 0;
            case 8, 9, 12, 13 -> 1;
            case 6, 7, 14, 15 -> 2;
            case 4, 5, 16, 17 -> 3;
            case 2, 3, 18, 19 -> 4;
            case 0, 1, 20, 21 -> 5;
            default -> 0;
        };
    }

    private int[] getNewStatAndScorePoints(String button, int statValue, int scorePoints) {
        return switch (button) {
            case "+" -> new int[]{statValue + 1, scorePoints - abilityCostPlus(statValue)};
            case "-" -> new int[]{statValue - 1, scorePoints + abilityCostMinus(statValue)};
            default -> new int[]{};
        };
    }

    private String getAbilityPrefix(int abilityValue) {
        if (abilityValue >= 10) {
            return "+";
        } else {
            return "-";
        }
    }

    @Override
    public List<Calculator> getAllById(long id) {
        return calculatorRepository.getAllById(id);
    }

    @Override
    public void setDefaultAbilityScoreValues(long id) {
        calculatorRepository.setDefaultAbilityScoreValues(id);
    }

    @Override
    public void getNewAbilityScoresSet() {
        Calculator newAbilityScoresSet = Calculator.builder()
                .abilityScorePoints(15)
                .strengthValue(10)
                .strengthPrefix("+")
                .strengthModifier(0)
                .dexterityValue(10)
                .dexterityPrefix("+")
                .dexterityModifier(0)
                .constitutionValue(10)
                .constitutionPrefix("+")
                .constitutionModifier(0)
                .intelligenceValue(10)
                .intelligencePrefix("+")
                .intelligenceModifier(0)
                .wisdomValue(10)
                .wisdomPrefix("+")
                .wisdomModifier(0)
                .charismaValue(10)
                .charismaPrefix("+")
                .charismaModifier(0)
                .build();

        calculatorRepository.save(newAbilityScoresSet);
    }

    @Transactional
    public Calculator saveAbilityScores(long characterId) {
        List<Calculator> calculator = calculatorRepository.getAllById(characterId);

        Calculator characterAbilityScores = Calculator.builder()
                .strengthValue(calculator.get(0).getStrengthValue())
                .strengthPrefix(calculator.get(0).getStrengthPrefix())
                .strengthModifier(calculator.get(0).getStrengthModifier())
                .dexterityValue(calculator.get(0).getDexterityValue())
                .dexterityPrefix(calculator.get(0).getDexterityPrefix())
                .dexterityModifier(calculator.get(0).getDexterityModifier())
                .constitutionValue(calculator.get(0).getConstitutionValue())
                .constitutionPrefix(calculator.get(0).getConstitutionPrefix())
                .constitutionModifier(calculator.get(0).getConstitutionModifier())
                .intelligenceValue(calculator.get(0).getIntelligenceValue())
                .intelligencePrefix(calculator.get(0).getIntelligencePrefix())
                .intelligenceModifier(calculator.get(0).getIntelligenceModifier())
                .wisdomValue(calculator.get(0).getWisdomValue())
                .wisdomPrefix(calculator.get(0).getWisdomPrefix())
                .wisdomModifier(calculator.get(0).getWisdomModifier())
                .charismaValue(calculator.get(0).getCharismaValue())
                .charismaPrefix(calculator.get(0).getCharismaPrefix())
                .charismaModifier(calculator.get(0).getCharismaModifier())
                .build();

        calculatorRepository.save(characterAbilityScores);

        return characterAbilityScores;
    }




    @Override
    public void strengthButton(CalculatorDto calculatorDto, Calculator calculatorStrength,long id) {

        int[] strengthStatAndScorePoints = getNewStatAndScorePoints(
                calculatorDto.getStrengthButton(),
                calculatorStrength.getStrengthValue(),
                calculatorStrength.getAbilityScorePoints());

        int abilityScorePoints = strengthStatAndScorePoints[1];
        int strengthValue = strengthStatAndScorePoints[0];
        String strengthPrefix = getAbilityPrefix(strengthStatAndScorePoints[0]);
        int strengthModifier = getAbilityModifier(strengthStatAndScorePoints[0]);

        calculatorRepository.strengthSetIntoDB(abilityScorePoints, strengthValue, strengthPrefix, strengthModifier,id);
    }

    @Override
    public void dexterityButton(CalculatorDto calculatorDto, Calculator calculatorDexterity,long id) {

        int[] dexterityStatAndScorePoints = getNewStatAndScorePoints(
                calculatorDto.getDexterityButton(),
                calculatorDexterity.getDexterityValue(),
                calculatorDexterity.getAbilityScorePoints());

        int abilityScorePoints = dexterityStatAndScorePoints[1];
        int dexterityValue = dexterityStatAndScorePoints[0];
        String dexterityPrefix = getAbilityPrefix(dexterityStatAndScorePoints[0]);
        int dexterityModifier = getAbilityModifier(dexterityStatAndScorePoints[0]);

        calculatorRepository.dexteritySetIntoDB(abilityScorePoints, dexterityValue, dexterityPrefix, dexterityModifier,id);
    }

    @Override
    public void constitutionButton(CalculatorDto calculatorDto, Calculator calculatorConstitution,long id) {

        int[] constitutionStatAndScorePoints = getNewStatAndScorePoints(
                calculatorDto.getConstitutionButton(),
                calculatorConstitution.getConstitutionValue(),
                calculatorConstitution.getAbilityScorePoints());

        int abilityScorePoints = constitutionStatAndScorePoints[1];
        int constitutionValue = constitutionStatAndScorePoints[0];
        String constitutionPrefix = getAbilityPrefix(constitutionStatAndScorePoints[0]);
        int constitutionModifier = getAbilityModifier(constitutionStatAndScorePoints[0]);

        calculatorRepository.constitutionSetIntoDB(abilityScorePoints, constitutionValue, constitutionPrefix, constitutionModifier,id);
    }

    @Override
    public void intelligenceButton(CalculatorDto calculatorDto, Calculator calculatorIntelligence,long id) {

        int[] intelligenceStatAndScorePoints = getNewStatAndScorePoints(
                calculatorDto.getIntelligenceButton(),
                calculatorIntelligence.getIntelligenceValue(),
                calculatorIntelligence.getAbilityScorePoints());

        int abilityScorePoints = intelligenceStatAndScorePoints[1];
        int intelligenceValue = intelligenceStatAndScorePoints[0];
        String intelligencePrefix = getAbilityPrefix(intelligenceStatAndScorePoints[0]);
        int intelligenceModifier = getAbilityModifier(intelligenceStatAndScorePoints[0]);

        calculatorRepository.intelligenceSetIntoDB(abilityScorePoints, intelligenceValue, intelligencePrefix, intelligenceModifier,id);

    }

    @Override
    public void wisdomButton(CalculatorDto calculatorDto, Calculator calculatorWisdom,long id) {
        int[] wisdomStatAndScorePoints = getNewStatAndScorePoints(
                calculatorDto.getWisdomButton(),
                calculatorWisdom.getWisdomValue(),
                calculatorWisdom.getAbilityScorePoints());

        int abilityScorePoints = wisdomStatAndScorePoints[1];
        int wisdomValue = wisdomStatAndScorePoints[0];
        String wisdomPrefix = getAbilityPrefix(wisdomStatAndScorePoints[0]);
        int wisdomModifier = getAbilityModifier(wisdomStatAndScorePoints[0]);

        calculatorRepository.wisdomSetIntoDB(abilityScorePoints, wisdomValue, wisdomPrefix, wisdomModifier,id);

    }

    @Override
    public void charismaButton(CalculatorDto calculatorDto, Calculator calculatorCharisma,long id) {
        int[] charismaStatAndScorePoints = getNewStatAndScorePoints(
                calculatorDto.getCharismaButton(),
                calculatorCharisma.getCharismaValue(),
                calculatorCharisma.getAbilityScorePoints());

        int abilityScorePoints = charismaStatAndScorePoints[1];
        int charismaValue = charismaStatAndScorePoints[0];
        String charismaPrefix = getAbilityPrefix(charismaStatAndScorePoints[0]);
        int charismaModifier = getAbilityModifier(charismaStatAndScorePoints[0]);

        calculatorRepository.charismaSetIntoDB(abilityScorePoints, charismaValue, charismaPrefix, charismaModifier,id);

    }


}
