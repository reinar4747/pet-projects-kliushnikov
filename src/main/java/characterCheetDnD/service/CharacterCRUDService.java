package characterCheetDnD.service;

import characterCheetDnD.models.Armor;
import characterCheetDnD.models.GameCharacter;
import characterCheetDnD.models.UserAccount;
import characterCheetDnD.models.Weapon;

import java.util.List;

public interface CharacterCRUDService {
    //-------------CRUD----------------

    void createNewCharacter();

    List<GameCharacter> getCharactersById(long characterId);
    List<GameCharacter> getCharactersByUserId(UserAccount userAccount);
    void onAndOffCharacterById(long id, boolean flag);


    List<GameCharacter> getDefaultCharacter();

    void setDefaultCharactersValues();
    void saveCharacter(long characterId);
    List<GameCharacter> character();
    void logicalDeleteCharacterById(boolean flag, long characterId);
    //------------race and class --------------

    void setRaceIntoDB(String race);

    void setClassIntoDB(String characterClass);
    //------------------ LvL and Experience and hitPoints------------
    void setLvLIntoDB(int LvL,long characterId);
    void setExperienceIntoDB(int experience,long characterId);
    void setHitPointsIntoDB(int hitPoints,long characterId);

    //-------------------weapon ------------------
    List<Weapon> getWeaponById(long weaponId);

    void setWeaponIntoDB(String weapon,long characterId);

    List<Weapon> getAllRangedWeapon();

    List<Weapon> getWeaponByWeight(String weight);
    List<Weapon> getWeaponByEquipped(String equipped);
    //---------------------armor --------------------
    List<Armor> getArmorById(long armorId);
    void setArmourIntoDB(String armour,long characterId);

    List<Armor> getAllByArmorWeight(String weight);

    //------------------ name----------------------
    void setNameIntoDB(String name,long id);

    //--------------------inventory-----------------
    void addItemIntoInventory(String newItem,long characterId);


}
