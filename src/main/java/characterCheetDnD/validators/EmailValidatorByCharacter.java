package characterCheetDnD.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

@Component("emailRegex")
public class EmailValidatorByCharacter implements EmailValidator {
    private Pattern pattern;

    @Override
    public String validate(String email) {
        if (!pattern.matcher(email).find()) {
            return "В почте отсутствует '@' \n ";
        }
        return "";
    }


    @Autowired //ставят на поля, на конструкторы и на сеттеры
    public void setPattern(@Value("${validator.email.regex}") String pattern) {
        this.pattern = Pattern.compile(pattern);
    }
}
