package characterCheetDnD.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("passwordByLength")
public class PasswordValidatorByLength implements PasswordValidator {
    private int minLength;

    @Autowired
    public PasswordValidatorByLength(@Value("${validator.password.length}") int minLength) {
        this.minLength = minLength;
    }

    @Override
    public String validate(String password,String passwordRepeat) {
        if ((password.length() < minLength)) {

            return String.format("Password too short! %d minimum - %d \n",password.length(),minLength);
        }
        return "";
    }

}
