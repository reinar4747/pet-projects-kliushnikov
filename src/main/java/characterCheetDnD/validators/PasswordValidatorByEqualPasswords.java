package characterCheetDnD.validators;

import org.springframework.stereotype.Component;

@Component("passwordByEqualsPasswords")
public class PasswordValidatorByEqualPasswords implements PasswordValidator{
    @Override
    public String validate(String password, String passwordRepeat) {
        if (!(password.equals(passwordRepeat))){
            return "Passwords don't equals!!! \n";
        }

        return "";
    }
}
