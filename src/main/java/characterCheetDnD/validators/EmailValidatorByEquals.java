package characterCheetDnD.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import characterCheetDnD.repositories.UserAccountRepository;

@Component("emailByEquals")
public class EmailValidatorByEquals implements EmailValidator {
    UserAccountRepository userAccountRepository;

    @Autowired
    public EmailValidatorByEquals(UserAccountRepository userAccountRepository) {
        this.userAccountRepository = userAccountRepository;
    }

    @Override
    public String validate(String email) {
//        if (userAccountRepository.getAllByEmail(email).size() != 0) {
//
//            return "Пользователь с таким email уже существует! \n ";
//        }
        if (userAccountRepository.getAllByEmailAndUserDeleted(email,false).size() != 0) {

            return "Пользователь с таким email уже существует! \n ";
        }

        return "";
    }
}
