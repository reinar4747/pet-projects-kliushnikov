package characterCheetDnD.validators;

import org.springframework.stereotype.Component;

@Component("passwordByCharacters")
public class PasswordValidatorByCharacters implements PasswordValidator {
    @Override
    public String validate(String password,String passwordRepeat) {

        if (!((password.contains("!") || password.contains("&") || (password.contains("#"))))) {
            return "Password don't contains ! or & or # \n";
        }

        return "";
    }
}
