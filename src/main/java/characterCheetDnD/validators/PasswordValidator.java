package characterCheetDnD.validators;

public interface PasswordValidator {
    String validate(String password,String passwordRepeat);
}
