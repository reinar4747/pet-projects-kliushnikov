package characterCheetDnD.validators;
import org.springframework.stereotype.Component;

@Component("passwordByUpperCase")
public class PasswordValidatorByUpperCase implements PasswordValidator{
    @Override
    public String validate(String password,String passwordRepeat) {
        int flag = 0;
        for (char point : password.toCharArray()) {
            if (Character.isUpperCase(point)) {
                flag++;
                break;
            }
        }
        if (flag == 0){
            return "Password doesn't has Upper letters \n";
        }else {
            return "";
        }
    }
}
