package characterCheetDnD.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CalculatorDto {

    private String strengthButton;
    private String dexterityButton;
    private String constitutionButton;
    private String intelligenceButton;
    private String wisdomButton;
    private String charismaButton;
    private String saveAbilityScores;

}
